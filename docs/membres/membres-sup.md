## Enseignant.e.s - Chercheur.euse.s impliqué.e.s

<br/>

::cards::

- title: "Isabelle Debled-Rennesson"
  content: "Isabelle est Professeure d'informatique à l'Université de Lorraine, elle enseigne à l'IUT Nancy Charlemagne et a été à l'origine de la création de la formation ISN dans l'académie de Nancy Metz et des journées SNT-NSI (ex-ISN). Isabelle est par ailleurs Vice-Présidente enseignement-formation de la Société Informatique de France."
  image: ../img/photos/IDR_HDR_08.jpg
  url: "https://members.loria.fr/IDebledRennesson/"

- title: "Marie Duflot-Kremer"
  content: "Marie est Maîtresse de conférences en informatique à l'Université de Lorraine et enseigne à la faculté des sciences et techniques. Marie est très impliquée dans la médiation scientifique et la formation des enseignant.e.s à la discpline NSI."
  image: ../img/photos/mariepro2-282x300.jpg
  url: "https://members.loria.fr/MDuflot/"

- title: "Nazim Fatès"
  content: "Nazim est Chargé de Recherche à l'Inria Nancy Grand Est. Son domaine de recherche correspond aux systèmes complexes. Il intervient depuis plusieurs années dans l'organisation de la journée académique NSI-SNT (ex-ISN)."
  image: ../img/photos/nazim.png
  url: "https://members.loria.fr/nazim.fates/"

- title: "David Langlois"
  content: "David est Maître de conférences en informatique à l'Université de Lorraine et enseigne à l'Institut National Supérieur du Professorat et de l'Education. David a participé à la formation des enseignant.e.s d'ISN et est à l'origine de celle des enseignant.e.s de NSI."
  image: ../img/icons/009-hermes.png
  url: "https://members.loria.fr/DLanglois/"

- title: "Yannick Parmentier"
  content: "Yannick est Maître de conférences en informatique à l'Université de Lorraine et enseigne à l'Institut National Supérieur du Professorat et de l'Education. Yannick a participé à la formation des enseignant.e.s d'ISN et participe à celle des enseignant.e.s de NSI. Yannick coordonne les journées SNT-NSI depuis 2023."
  image: ../img/photos/IMG_1297.jpg
  url: "https://sourcesup.renater.fr/www/tulipa/yannick"

- title: "Laurent Pierron"
  content: "Laurent est ingénieur dans le service SED, qui aide les chercheurs à réaliser des expérimentations scientifiques. Il a notamment réalisé un logiciel de synthèse vocale à base de Deep Learning. <br/>Il participe, pour les aspects techniques, à la médiation scientifique avec le service communication. Dans ce cadre il a participé à la réalisation et la maintenance de l'exposition Homo Numericus et a co-développé avec Unity 3D le jeu Inside pour casque de réalité virtuelle Oculus Quest."
  image: ../img/photos/P1030124.jpeg
  url: ""

- title: "Vincent Thomas"
  content: "Vincent est Maître de conférences en informatique à l'Université de Lorraine et enseigne à l'Institut Universitaire de Technologie Nancy Charlemagne. Vincent anime régulièrement des ateliers lors des journées académiques NSI-SNT (ex-ISN)."
  image: ../img/photos/vincent_thomas.jpg
  url: "https://members.loria.fr/vincent.thomas/"

::/cards::

<small>Icones de la mythologie grecque réalisées par [max.icons](https://www.flaticon.com/authors/maxicons) et disponible sur [www.flaticon.com](https://www.flaticon.com).</small>

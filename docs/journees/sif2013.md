Les supports et des documents sont disponibles dans un fichier zip téléchargeable [ici](../pdf/journees-sif.zip).

## Jeudi 11 avril

* **Introduction des journées** - Colin de la Higuera, Université de Nantes
* **Bilan ISN en France : Point sur les formations et ouverture de la spécialité en Terminale Scientifique, projet pour les autres sections de terminale** – Laurent Chéno
* **Historique des tentatives d’introduction de l’enseignement de l'informatique dans le second degré** – Jacques Baudé
* **Point sur l’enseignement de l’informatique dans le second degré au niveau international** – Françoise Tort
* **L'informatique dans l'enseignement secondaire en Suisse** - Brice Canvel
* **Enseigner l'informatique dans le second degré** - Gilles Dowek, ENS Cachan
* **Comment enseigner la science informatique à des professeurs en moins de 30 heures !** — Yann Busnel, Université de Nantes
* **Une formation initiale en informatique et sciences du numérique dans l'Académie de Grenoble à destination des professeurs de lycée : une mise en œuvre de "reconversion" scientifique** — Jean-Marc Vincent, Université Joseph Fourier, Grenoble
* **Des graphes et des images en Licence 1 scientifique générale: retour d'expérience et adaptation à l'ISN** — Samuel Thibault, Université Bordeaux 1
* **Init Prog ou Init Info en 1re année de licence ? Influence d'ISN** — Éric Wegrzynowski, Université Lille 1
* **Le nouveau programme d'informatique en CPGE** — Laurent Chéno
* **Enseignement de l'informatique dans les cursus d'ingénieur** — Pierre Rolin
* **Le C2i, une suite du B2i ou un bon complément à l'ISN ?** — Jean-Yves Jeannas
* **Que doit devenir le «SIL:O!» à l'ère post-ISN** — Thierry Viéville et les acteurs du «SIL:O!» 

## Vendredi 12 avril

Bilan et discussions sur les modalités de formation et les contenus enseignés à partir des retours des questionnaires - Isabelle Debled-Rennesson

- Formation à distance proposée par Limoges – Olivier Ruatta
- ATELIER A : Organisation du travail de la classe
- ATELIER B : Didactique de l'algorithmique
- ATELIER C : Gestion des projets
- ATELIER D : Evaluation par compétences
- Conception de ressources sur Eduscol - Gilles Ollivier 

## Utiliser Scratch

Voici ci-dessous quelques liens pour faire suite aux ateliers Scratch des journées ISN/EPI :

#### La communauté Scratch
  * [le site de la communauté Scratch](https://scratch.mit.edu/)
  * [Foire aux questions](http://scratch.mit.edu/help/faq)
  * [Aide en ligne](http://scratch.mit.edu/help)
  * [Forum éducation de Scratch](http://scratched.gse.harvard.edu/)
    
#### Chez les éditeurs
  * [Guide](http://www.editions-eyrolles.com/Livre/9782212141115/scratch-pour-les-kids) pour bien commencer (onglet "Compléments" en bas de page, lien "Compléments web", fichier "Pour bien commencer avec Scratch.pdf" dans l'archive) 
		
#### Quelques projets qui déménagent !
  * un très joli [jeu de plateforme](https://scratch.mit.edu/projects/91723110/)
  * [tic tac toe](https://scratch.mit.edu/projects/37306788/), joli, mais surtout montre comment on gère le survol de souris
  * [casse-tête](https://scratch.mit.edu/projects/95902058/)
  * [un autre](https://scratch.mit.edu/projects/87868336/)
    
#### La page médiation scientifique de Marie Duflot-Kremer (accessible [ici](http://www.loria.fr/~mduflotk/mediation.html)).

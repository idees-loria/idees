# Idees

Ce projet Gitlab contient les Sources du site web du groupe IDEES du Loria (https://idees.loria.fr).

Ce site est compilé automatiquement au moyen du logiciel [mkdocs](mkdocs.org) à partir de fichiers aux formats markdown (contenu des pages composant le site) et yaml (configuration des menus, plugins, thèmes).

## Installation locale
Pour compiler le site localement, il faut installer `mkdocs`, le thème `mkdocs-bootswatch` et le plugin `neoteroi`:

```python
pip install mkdocs
pip install mkdocs-bootswatch
pip install neoteroi-mkdocs
``` 

A noter: vous pouvez modifier les sources du site directement sur gitlab.inria.fr (via l'éditeur intégré), à chaque commit le site sera recompilé et redéployé.

*L'installation locale n'est utile que pour tester des modifications avant de les déployer.*

## Structure des sources

Les fichiers importants sont :

- `mkdocs.yml` : fichier de configuration contenant la définition des menus, du thème, des plugins, etc.
- `docs` : répertoire contenant les sources du site (fichiers markdown, pdf, png)
- `.gitlab-ci.yml` : fichier de configuration utilisé pour la compilation et le déploiement du site (ne pas modifier).

## Usage local

Pour compiler le site localement :

```bash
cd idees
mkdocs build
```

Le site statique est alors compilé et enregistré dans le répertoire `site/`.

## Contact

Yannick Parmentier <yannick.parmentier@loria.fr>
